import os, sys
import argparse
import wave
from PIL import Image

parser = argparse.ArgumentParser(description="Decode images into WAV audio files",epilog="The program produces 8 bit unsigned WAV files")
parser.add_argument("file",metavar="IMGFILE",help="Path to the image file")
parser.add_argument("rate",metavar="RATE",help="Sample rate")
parser.add_argument("-o",metavar="OUT",dest="out",help="Output file, will use input file's name if not specified")
args = parser.parse_args()

fn = ""
if args.out:
    fn = args.out
else:
    fn = args.file.split(".")[0]+".wav"

wav = wave.open(fn,"wb")

wav.setsampwidth(1) # 8 bit
wav.setframerate(int(args.rate)) # User specified sample rate
wav.setnchannels(1) # Mono

img = Image.open(args.file)
rgbimg = img.convert("RGB")
for x in range(img.size[0]):
    for y in range(img.size[1]):
        r,g,b = rgbimg.getpixel((x,y))
        print("P: {}, PS: {}, V: {}".format(x,y,g),end="\r")
        wav.writeframesraw(bytes([g]))