import os, sys
import math
import wave
import argparse
from PIL import Image

parser = argparse.ArgumentParser(description="Encode WAV audio files into images",epilog="The audio file must be 8 bit unsigned!")
parser.add_argument("file",metavar="WAVFILE",help="Path to the WAV file")
parser.add_argument("ppp",metavar="PPP",help="Pixels per period/column, 1024 is alright")
parser.add_argument("-o",metavar="OUT",dest="out",help="Output file, will use input file's name if not specified")
args = parser.parse_args()
wav = wave.open(args.file,"rb")
#ppp = int(sys.argv[2])
ppp = int(args.ppp)

columns = math.ceil(wav.getnframes()/ppp)
#print(wav.getnframes())

img = Image.new("RGB",(columns,ppp),"black")
pixels = img.load()

wx = 0
wy = 0

print("Periods needed: "+str(columns))

for x in range(columns):
    for y in range(ppp):
        val = int.from_bytes(wav.readframes(1),"little") # Read 8 bit unsigned LE byte
        print("P: {}, PS: {}, V: {}".format(x,y,val),end="\r")
        pixels[x,y] = (0,val,0)
        wy += 1
    wx += 1

print("Wrote {} periods and {} samples".format(wx,wy))

fn = ""
if args.out:
    fn = args.out
else:
    fn, _ = os.path.splitext(args.file)

img.save(fn+".png","PNG")